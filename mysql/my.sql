-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 21 2016 г., 03:45
-- Версия сервера: 5.6.12-log
-- Версия PHP: 5.4.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `my`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `attraction` text NOT NULL,
  `lounges` text NOT NULL,
  `living_cost` int(11) NOT NULL,
  `food_cost` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `add_id` int(11) NOT NULL,
  `img_file` varchar(150) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `author_id` (`author_id`),
  KEY `author_id_2` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`city_id`, `city`, `description`, `attraction`, `lounges`, `living_cost`, `food_cost`, `author_id`, `add_id`, `img_file`) VALUES
(1, 'Москва', 'Столица Российской Федерации, город федерального значения, административный центр Центрального федерального округа и центр Московской области, в состав которой не входит. Крупнейший по численности населения город России и её субъект — 12 330 126 чел. (2016), самый населённый из городов, полностью расположенных в Европе, входит в первую десятку городов мира по численности населения.', 'Москва расположена на реке Москве, по городской территории протекают и более мелкие реки, поэтому город располагает большим количеством мостов. Самые известные из них: Большой Каменный мост и Малый Каменный мост, Крымский мост, Живописный мост. Исторический центр Москвы — Московский Кремль. Свой нынешний вид стены и башни крепости приобрели ещё в XVII веке. В наше время Кремль служит резиденцией Президента России, на его территории находятся многочисленные храмы и музеи, ансамбль Московского Кремля входит в список всемирного наследия ЮНЕСКО. \r\n', 'В Москве масса возможностей для детского досуга. Самые известные места, которые москвичи и гости столицы посещают с детьми — Московский зоопарк, Московский планетарий, музей занимательных наук Экспериментаниум, Биологический музей им. К. А. Тимирязева. В 2015 году на территории ВДНХ был открыт океанариум «Москвариум».Самым большим крытым парком развлечений и аттракционов является Happylon Magic Park. А в Нагатинской пойме совместно с компанией DreamWorks строится новый парк развлечений мирового уровня.', 2000, 1000, 1, 1, 'images\\img1.jpg'),
(2, 'Казань', 'Каза́нь (тат. Казан, Qazan) — город в Российской Федерации, столица Республики Татарстан, крупный порт на левом берегу реки Волги, при впадении в неё реки Казанки. Один из крупнейших религиозных, экономических, политических, научных, образовательных, культурных и спортивных центров России. Казанский кремль входит в число объектов Всемирного наследия ЮНЕСКО.', '«Визитной карточкой» Казани является вид со стороны реки Казанка на главную городскую площадь Тысячелетия и главную городскую достопримечательность Кремль: «летающая тарелка» цирка, пирамида культурно-развлекательного комплекса и находящиеся в обрамлении кремлёвских стен главная соборная Мечеть Кул-Шариф, падающая Башня Сююмбике, Благовещенский собор образуют эффектный силуэт, ставший символом города. Наименование города отражено в современном здании Центра семьи «Казан» в форме казана на противоположном Кремлю берегу Казанки.\r\n', 'Узнаваемые места в Казани — площадь Тысячелетия с древним Кремлем, здание цирка с его оригинальным куполом, башни соборов. Казанский Кремль на своей территории вмещает Музей ислама, Музей естественной истории, Мемориал Великой Отечественной войны, Мавзолей казанских ханов, галерею Эрмитаж-Казань, несколько соборов, башню Сююмбике, главную мечеть Татарстана Кул-Шариф.', 1500, 800, 2, 2, 'images\\img2.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(30) NOT NULL,
  `user_pass` varchar(32) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_pass`) VALUES
(1, 'mylogin', '202cb962ac59075b964b07152d234b70'),
(2, 'vadim', '202cb962ac59075b964b07152d234b70');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
